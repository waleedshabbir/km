import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthService } from '../../providers/auth-service';
import { GlobalVariable } from '../../app/global';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  submitAttempt: boolean = false;
  loginForm: FormGroup;

  constructor(public navCtrl: NavController,
    public events: Events,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public menu: MenuController,
    public as: AuthService,
    public formBuilder: FormBuilder,
    private nativeStorage: NativeStorage,
    private globals: GlobalVariable) {

    menu.swipeEnable(false);

    this.loginForm = formBuilder.group({
      username: [''],
      password: ['']
    });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  async login() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log(' Some values were not given or were incorrect, please fill them');
    } else {
      try {
        const response = await this.as.login(this.loginForm.value);
        console.log(response);
        if (response.ERROR_CODE == "1") {

          let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: response.ERROR_MSG,
            buttons: ['Tentar novamente']
          })
          alert.present();
        }
        else {
          let loginData = {
            username: this.loginForm.value.username,
            password: this.loginForm.value.password
          }
          this.saveToLocalStorage("loginData", loginData);
          this.saveToLocalStorage("loginResponse", response);
          this.globals.loginData = loginData;
          this.globals.loginResponse = response;
          this.globals.username = this.loginForm.value.username;
          this.events.publish('articleMenu:populate', response);
          this.navCtrl.setRoot("Child");
        }

      } catch (error) {
        console.error("error logging in: ", error);
      }
    }
  }

  saveToLocalStorage(key: string, value: any) {
    this.nativeStorage.setItem(key, {
      [key]: value,
      username: this.loginForm.value.username
    });
  }
}
