import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Weekly } from './weekly';

@NgModule({
  declarations: [
    Weekly,
  ],
  imports: [
    IonicPageModule.forChild(Weekly),
  ],
  exports: [
    Weekly
  ]
})
export class WeeklyModule {}
