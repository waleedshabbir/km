import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { GlobalVariable } from '../../app/global';

@IonicPage()
@Component({
  selector: 'page-weekly',
  templateUrl: 'weekly.html',
})
export class Weekly {
  @ViewChild('mySlider') slider: Slides;
  @ViewChild('tabSlider') tabSlider: Slides;

  weeklyData: any;
  days = ["SEGUNDA", "TERÇA", "QUARTA", "QUINTA", "SEXTA"];
  selected = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private globals: GlobalVariable) {
    this.weeklyData = navParams.get("weeklyData");
    console.log(this.weeklyData);
  }

  select(index) {
    console.log(index);
    this.selected = index;

    this.slider.slideTo(index, 500);
  }

  onSlideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    if (currentIndex === 3) this.selected = 3;
    
    if (currentIndex === 2) this.selected = 2;

    if (currentIndex === 1) this.selected = 1;

    if (currentIndex === 0) this.selected = 0;
    
    this.tabSlider.slideTo(currentIndex, 500);

  }
  tabsChanged() {
    let currentIndex = this.tabSlider.getActiveIndex();
    // console.log("tabSlider", currentIndex);
    this.slider.slideTo(currentIndex, 500);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Weekly');
  }

}
