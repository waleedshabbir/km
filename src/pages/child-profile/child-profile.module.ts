import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChildProfile } from './child-profile';

@NgModule({
  declarations: [
    ChildProfile,
  ],
  imports: [
    IonicPageModule.forChild(ChildProfile),
  ],
  exports: [
    ChildProfile
  ]
})
export class ChildProfileModule {}
