import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-child-profile',
  templateUrl: 'child-profile.html',
})
export class ChildProfile {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let data: Observable<any>;
    data = navParams.get('data');
    data.subscribe(result => {console.log(result);});    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChildProfile');
  }

}
