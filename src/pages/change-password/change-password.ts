import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePassword {
  password: any;
  reconfirmPassword: any;
  passwordSame: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private as: AuthService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePassword');
  }

  async changePassword() {
    if (this.password === this.reconfirmPassword) {
      let loader = this.loadingCtrl.create({
        content: "Alterando a senha",
        dismissOnPageChange: true
      });
      loader.present();
      this.passwordSame = true;
      const response = await this.as.changePassword(this.password);
      console.log(response);
      loader.dismiss();
      if (response.ERROR_CODE == "0") {
        let alert = this.alertCtrl.create({
          title: response.msg,
          buttons: ["Está bem"]
        });
        alert.present();
        alert.onDidDismiss(
          () => this.navCtrl.setRoot("Child")
        )
      } else {
        loader.dismiss();
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: response.ERROR_MSG,
          buttons: ['Tentar novamente']
        });
        alert.present();
      }
    } else {
      this.passwordSame = false;
    }
  }

}
