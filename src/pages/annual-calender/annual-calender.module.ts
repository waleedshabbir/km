import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnnualCalender } from './annual-calender';

@NgModule({
  declarations: [
    AnnualCalender,
  ],
  imports: [
    IonicPageModule.forChild(AnnualCalender),
  ],
  exports: [
    AnnualCalender
  ]
})
export class AnnualCalenderModule {}
