import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataService } from "../../providers/data-service";
import { Subscription } from "rxjs";
import { GlobalVariable } from "../../app/global";

@IonicPage()
@Component({
  selector: 'page-annual-calender',
  templateUrl: 'annual-calender.html',
})
export class AnnualCalender {
  calenderSubscription: Subscription;
  calenderData;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public globals: GlobalVariable,
    public ds: DataService) {

    let studentUID = navParams.get('studentUID')
    let studentSchooID = navParams.get('studentSchooID')
    console.log("studentSchooID", studentSchooID)
    this.calenderSubscription = this.ds.getSchoolCalender(studentUID, studentSchooID)
      .subscribe(res => {
        console.log(res)
        this.calenderData = res
      }, err => console.error(err))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnnualCalender');
  }

  /* ionViewWillLoad() {
    this.calenderSubscription = this.ds.getSchoolCalender(studentUID, studentSchooID)
      .subscribe(res => {
        console.log(res)
        this.calenderData = res
      }, err => console.error(err))
  } */
   ionViewWillLeave() {
     this.calenderSubscription.unsubscribe()
   }
}
