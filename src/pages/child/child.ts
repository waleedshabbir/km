import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController } from 'ionic-angular';
import { GlobalVariable } from '../../app/global';
import { DataService } from '../../providers/data-service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/switchMap';
import { AuthService } from "../../providers/auth-service";
import { NativeStorage } from "@ionic-native/native-storage";
// import 'rxjs/add/operator/forkJoin'

@IonicPage()
@Component({
  selector: 'page-child',
  templateUrl: 'child.html',
})
export class Child {
  students: Array<any> = [];
  mainData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public dS: DataService,
    private menu: MenuController,
    private globals: GlobalVariable,
    private nativeStorage: NativeStorage,
    private as: AuthService) {

    let data = globals.loginData;
    // this.students = globals.loginResponse.ALUNO;
    console.log("globals.loginResponse.ALUNO", globals.loginResponse.ALUNO)
  }

  async ionViewWillEnter() {
    let loader = this.loadingCtrl.create();
    loader.present();
    this.students = [];
    console.log(`this.students: ${this.students}`);
    let data = await this.nativeStorage.getItem("loginData")
    let response = await this.as.login(
      {
        username: data.loginData.username,
        password: data.loginData.password
      })
    this.students = response.ALUNO;
    console.log("response from login page", response)
    loader.dismiss()
    // .then(data => {
    //   console.log(data)
    //   this.as.login(
    //     {
    //       username: data.loginData.username,
    //       password: data.loginData.password
    //     }).then(response => {
    //       this.students = response.ALUNO;
    //     })
    // })
    // console.log("response from login page", response)
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(true);
    console.log('ionViewDidLoad Child');
  }

  getStudentProfile(studentUID, studentSchooID, student) {
    console.log('getting student profile');
    this.startLoader();
    this.globals.student = student;
    /* if (this.globals.childPageData[studentUID]) {
      console.log("data already loaded");
      this.navCtrl.push('Main', {
        mainData: this.globals.childPageData[studentUID],
        student: student
      });
    } else { */
    let profile: Observable<any>;
    // let messages: Observable<any>;
    let feedCount: Observable<any>;
    let documents: Observable<any>;
    // console.log(this.globals.loginData);
    profile = this.dS.getStudentProfile(studentUID, studentSchooID);
    // messages = this.dS.getStudentMessages(studentUID, studentSchooID);
    feedCount = this.dS.getFeedCount(studentUID, studentSchooID);
    documents = this.dS.getDocuments(studentUID, studentSchooID);

    Observable.forkJoin(profile, /*  messages,  */ feedCount, documents)
      .subscribe(response => {
        // console.log(response);
        response.map(
          (resp) => {
            if (resp == null) resp == undefined;
          }
        );
        this.globals.childPageData[studentUID] = response;
        console.log("this.globals.childPageData[studentUID]", this.globals.childPageData[studentUID]);
        this.mainData = response;
        this.navCtrl.push('Main', {
          mainData: response,
          student: student
        });
      }, error => console.error("error in forkjoin:", error));
    /* } */
  }

  startLoader() {
    let loader = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loader.present();

  }

}
