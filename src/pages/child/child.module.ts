import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Child } from './child';

@NgModule({
  declarations: [
    Child,
  ],
  imports: [
    IonicPageModule.forChild(Child),
  ],
  exports: [
    Child
  ]
})
export class ChildModule {}
