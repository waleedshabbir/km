import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-bills',
  templateUrl: 'bills.html',
})
export class Bills {

  bills: any;

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  private iab: InAppBrowser) {

    this.bills = navParams.get("bills");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Bills');
  }

  downloadBill(pdf) {
    const browser = this.iab.create('http://' + pdf, '_system', 'location=no');
  }

}
