import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Bills } from './bills';

@NgModule({
  declarations: [
    Bills,
  ],
  imports: [
    IonicPageModule.forChild(Bills),
  ],
  exports: [
    Bills
  ]
})
export class BillsModule {}
