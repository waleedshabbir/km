import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { GlobalVariable } from '../../app/global';
import { DataService } from '../../providers/data-service';
import { Observable, Subscription } from 'rxjs/Rx';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class Main {
  @ViewChild('mySlider') slider: Slides;

  profile: any;
  newsfeed: any;
  messages: any;
  feedCount: any;
  student: any;
  documents: any;
  weeklyCalendar: any;
  billsCount: any;

  mainData: any;
  public indicator = null;
  lido = [];

  ngAfterViewInit() {
    this.indicator = document.getElementById("indicator");
  }

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public dS: DataService,
    private globals: GlobalVariable,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private photoViewer: PhotoViewer,
    public menuCtrl: MenuController,
    private iab: InAppBrowser) {

    this.menuCtrl.swipeEnable(false);
    this.mainData = navParams.get('mainData');
    this.student = navParams.get('student');

    this.dS.getStudentMessages(this.student.IDALUNO, this.student.MATRICULA)
      .subscribe((resp) => {
        console.log("this.newsfeed", resp)
        this.newsfeed = resp.OCORRENCIAS
        for (let news of this.newsfeed) {
          this.lido.push(news.LIDO)
        }
        console.log(this.lido)
      })
    this.cacheDataFromServer();

    if (this.mainData[0]) this.profile = this.mainData[0]['DADO'];
    // if (this.mainData[1]) this.newsfeed = this.mainData[1]['OCORRENCIAS'];
    if (this.mainData[1]) this.feedCount = this.mainData[1];
    if (this.mainData[2]) this.documents = this.mainData[2]['DOCUMENTOS'];

  }
  panEvent($event) {
    // console.log(e);
    if (((($event.touches.startX - $event.touches.currentX) <= 100) || (($event.touches.startX - $event.touches.currentX) > 0)) && (this.slider.isBeginning() || this.slider.isEnd())) {
      console.log("interdit Direction");
    }
    else {
      console.log("OK Direction");
      this.indicator.style.webkitTransform = 'translate3d(' + (-($event.translate) / 4) + 'px,0,0)';
    }
  }
  onSlideChanged() {

    let currentIndex = this.slider.getActiveIndex();
    // console.log('current index: ', currentIndex);
    if (currentIndex === 6) this.indicator.style.webkitTransform = 'translate3d(0%,0,0)';
    // this.selected = 2;
    // console.log(currentIndex);
    if (currentIndex === 5) this.indicator.style.webkitTransform = 'translate3d(400%,0,0)';
    // this.selected = 2;
    // console.log(currentIndex);
    if (currentIndex === 4) this.indicator.style.webkitTransform = 'translate3d(300%,0,0)';
    // this.selected = 2;
    // console.log(currentIndex);
    if (currentIndex === 3) this.indicator.style.webkitTransform = 'translate3d(200%,0,0)';
    // this.selected = 2;
    // console.log(currentIndex);
    if (currentIndex === 2) this.indicator.style.webkitTransform = 'translate3d(100%,0,0)';
    // this.selected = 1;
    // console.log(currentIndex);
    if (currentIndex === 1) this.indicator.style.webkitTransform = 'translate3d(0%,0,0)';
    // this.selected = 0;
    // console.log(currentIndex);
    if (currentIndex === 0) this.indicator.style.webkitTransform = 'translate3d(400%,0,0)';
    // this.selected = 0;
    // console.log(currentIndex);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Main');
  }

  b1() {
    this.slider.slideTo(1);
    this.indicator.style.webkitTransform = 'translate3d(0%,0,0)';
  }

  b2() {
    this.slider.slideTo(2);
    this.indicator.style.webkitTransform = 'translate3d(100%,0,0)';
  }

  b3() {
    this.slider.slideTo(3);
    this.indicator.style.webkitTransform = 'translate3d(200%,0,0)';
  }

  b4() {
    this.slider.slideTo(4);
    this.indicator.style.webkitTransform = 'translate3d(300%,0,0)';
  }

  b5() {
    this.slider.slideTo(5);
    this.indicator.style.webkitTransform = 'translate3d(400%,0,0)';
  }


  documentAttachment(file) {
    console.log("PhotoViewer called");
    console.log(file);
    console.log(file.IMG);


    if (file.IMG) {
      this.photoViewer.show('http://' + file.IMG);
    }
    else {
      //this.photoViewer.show('http://www.kmescolar.net' + file.IMG);

      //  'https://docs.google.com/viewer?url=' + encodeURIComponent(file.PDF);
      // let link = 'https://docs.google.com/viewer?url=' + encodeURIComponent('http://lahore.comsats.edu.pk/Downloads/TimeTable/Classes-UG-SP17-20170523-1630.pdf');
      // console.log(link);
      const browser = this.iab.create('http://' + file.PDF, '_system', 'location=no');
      //browser.show();
    }
  }

  getWeeklySchedule() {
    console.log('getting weekly schedule of student');
    // let weeklySchedule: Observable<any>;

    // weeklySchedule = this.dS.getQuadro(this.student.IDALUNO, this.student.MATRICULA);
    // weeklySchedule.subscribe(response => {
    //   this.navCtrl.push("Weekly", { weeklyData: response });
    // }, error => console.error(error));

    this.navCtrl.push("Weekly", { weeklyData: this.globals.mainPageData[0] });
  }

  async getGradeReports() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Selecione o ano');
    // const gradeReports = await this.dS.getGradeReports(this.student.IDALUNO, this.student.MATRICULA).toPromise();
    let gradeReports = this.globals.mainPageData[1];
    gradeReports.BOLETINS.map(
      (report) => {
        alert.addInput({
          type: 'radio',
          label: report.ANO,
          value: report,
          // checked: true
        });
      }
    );
    alert.addButton('CANCELAR');
    alert.addButton({
      text: 'BAIXAR',
      handler: report => {
        console.log(report.ANO);
        console.log(report.PDF);
        this.iab.create('http://' + report.PDF, '_system', 'location=no');
        // browser.show();
      }
    });
    alert.present();
  }

  async getAllBills() {
    try {
      // const bills = await this.dS.getAllBills(this.student.IDALUNO, this.student.MATRICULA).toPromise();
      const bills = this.globals.mainPageData[2];
      this.navCtrl.push("Bills", { bills: bills });
    } catch (error) {
      console.error(error);
    }
  }

  async getFinancialReport() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Selecione o ano');
    // const financialReport = await this.dS.getFinancialReports(this.student.IDALUNO, this.student.MATRICULA).toPromise();
    let financialReport = this.globals.mainPageData[3];
    financialReport.DOCUMENTOS.map(
      (report) => {
        alert.addInput({
          type: 'radio',
          label: report.ANO,
          value: report,
          // checked: true
        });
      }
    );
    alert.addButton('CANCELAR');
    alert.addButton({
      text: 'BAIXAR',
      handler: report => {
        console.log(report.ANO);
        console.log(report.PDF);
        this.iab.create('http://' + report.PDF, '_system', 'location=no');
        // browser.show();
      }
    });
    alert.present();
  }

  cacheDataFromServer() {
    console.log("cacheDataFromServer");
    // if (this.globals.mainPageData) {
    //   console.log("data already loaded");
    //   console.log(this.globals.mainPageData);
    // } else {
    this.billsCount = this.dS.getFinancialCounts(this.student.IDALUNO, this.student.MATRICULA)
    let weeklySchedule: Observable<any>;
    let gradeReports: Observable<any>;
    let bills: Observable<any>;
    let financialReport: Observable<any>;
    // console.log(this.globals.loginData);
    weeklySchedule = this.dS.getQuadro(this.student.IDALUNO, this.student.MATRICULA);
    gradeReports = this.dS.getGradeReports(this.student.IDALUNO, this.student.MATRICULA);
    bills = this.dS.getAllBills(this.student.IDALUNO, this.student.MATRICULA);
    financialReport = this.dS.getFinancialReports(this.student.IDALUNO, this.student.MATRICULA)

    Observable.forkJoin(weeklySchedule, gradeReports, bills, financialReport)
      .subscribe(
      (response) => {
        console.log(response);
        response.map(
          (resp) => {
            if (resp == null) resp == undefined;
          }
        );
        this.globals.mainPageData = response
      }, error => console.error("error in forkjoin:", error)
      );
    // }
  }

  goToAnnualCalender() {
    this.navCtrl.push('AnnualCalender',
      {
        studentUID: this.student.IDALUNO,
        studentSchooID: this.student.MATRICULA
      })
  }

  startLoader() {
    let loader = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loader.present();

  }

  async notficationReadUnread(lido, msgNumber, i) {

    if (this.lido[i] == '0') {
      this.lido[i] = '1'
    } else this.lido[i] = '0'
    console.log(this.lido)
    let notifResp = await this.dS.notificationReadUnread(lido, msgNumber, this.student.IDALUNO, this.student.MATRICULA).toPromise()
    console.log(notifResp)
    this.feedCount = await this.dS.getFeedCount(this.student.IDALUNO, this.student.MATRICULA).toPromise()
    /* Observable.forkJoin(
      this.dS.notificationReadUnread(lido, msgNumber, this.student.IDALUNO, this.student.MATRICULA),
      this.dS.getFeedCount(this.student.IDALUNO, this.student.MATRICULA),
    ).subscribe(resp => {
      console.log(resp)
      this.feedCount = resp[1]
    }, error => console.error(error)) */
    // this.dS.notificationReadUnread(lido, msgNumber, this.student.IDALUNO, this.student.MATRICULA)
    //   .subscribe(resp => {
    //     console.log(resp)
    //   }, error => console.error(error))
  }

}
