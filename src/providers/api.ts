import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";
import { GlobalVariable } from '../app/global';


@Injectable()
export class ApiProvider {

  private url = this.globals.url;
  constructor(public http: Http, private globals: GlobalVariable,) {
    console.log('Hello ApiProvider Provider');
  }

  get(endpoint: string, params?: any, options?: RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }

      options.search = !options.search && p || options.search;
    }

    return this.http.get(this.url + '/' + endpoint, options);
  }

  post(endpoint: string, body?: any, options?: RequestOptions) {
    console.log(this.url + '/' + endpoint);
    if (body) {
      let urlBody = new URLSearchParams();
      for (let k in body) {
        urlBody.set(k, body[k]);
      }
      console.log("urlBody", urlBody);
      return this.http.post(this.url + '/' +  endpoint, urlBody);
    } else {
      return this.http.post(this.url + '/' + endpoint, body);
    }

  }

  put(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint: string, options?: RequestOptions) {
    return this.http.delete(this.url + '/' + endpoint, options);
  }

  patch(endpoint: string, body: any, options?: RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  public returnResponse(res) {
    return res.json()
  }

  public catchError(error) {
    // console.log(error);
    return Observable.throw(/* error.json().error || */ error || "error in api");
    // return Promise.reject(error.json().error || error || "some error in http post");
  }

}
