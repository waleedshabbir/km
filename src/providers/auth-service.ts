import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { md5 } from './md5';
import { GlobalVariable } from '../app/global';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  constructor(public http: Http, private globals: GlobalVariable) {
    console.log('Hello AuthService Provider');
  }

  login(formData): Promise<any> {

    let url = this.globals.url +  "api_login.php";
    let login = formData.username;
    let password = formData.password;
    let school_path = this.globals.school_path;
    let hash = this.globals.school_hash;
    let locatalTime = this.getLocaTime();

    let hashCode = md5(password + login + locatalTime.toString() + hash + school_path)

    console.log("login", login);
    let body = new URLSearchParams();
    body.set('c', school_path);
    body.set('d', locatalTime.toString());
    body.set('l', login);
    body.set('s', hashCode)
    // body.set('page', page.toString());
    let headers = new Headers({ 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, search: body.toString() });

    console.log(body);

    return this.http.get(url, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.catchError);
  }

  changePassword(newPassword: string) {
    let url = this.globals.url + "api_senha.php";
    let school_path = this.globals.school_path;
    let hash = this.globals.school_hash;
    let locatalTime = this.getLocaTime();
    let hashCode = md5(newPassword + this.globals.username + locatalTime.toString() + hash + school_path);

    console.log("this.globals.username:", this.globals.username);
    console.log("newPassword", newPassword + " " + newPassword.toUpperCase());
    let body = new URLSearchParams();
    body.set('c', school_path);
    body.set('d', locatalTime.toString());
    body.set('l', this.globals.username);
    body.set('s', newPassword);
    body.set('h', hashCode);

    let headers = new Headers({ 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.catchError);
  }

  private getLocaTime() {
    let locatalTime = new Date().getTime() / 1000

    return parseInt(locatalTime.toString());
  }

  private logResponse(res: Response) {
    console.log(res);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any) {
    // console.log(error);
    // return Observable.throw(error.json().error || error || "some error in http get");
    return Promise.reject(error.json().error || error || "some error in http post");
  }
}
