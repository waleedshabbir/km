import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { md5 } from './md5';
import { GlobalVariable } from '../app/global';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
// import 'rxjs/add/operator


@Injectable()
export class DataService {

  headers: any;

  constructor(
    private http: Http,
    private globals: GlobalVariable,
  ) {

    console.log('Hello DataSerive Provider');

    this.headers = new Headers({ 'Access-Control-Allow-Origin': '*', 'Content-Type': 'application/json' });
  }

  getStudentProfile(studentUID, studentSchooID) {

    let url = this.globals.url + "api_aluno.php";

    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }

  getStudentMessages(studentUID, studentSchooID) {

    let url = this.globals.url + "api_ocorrencias.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    let response = this.http.get(url, options)
    return response
      .map(this.extractData)
      .catch(this.catchError)
  }

  getFeedCount(studentUID, studentSchooID) {

    let url = this.globals.url + "api_contagem.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }



  getDocuments(studentUID, studentSchooID) {

    let url = this.globals.url + "api_documentos.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }


  getQuadro(studentUID, studentSchooID) {

    let url = this.globals.url + "api_quadro.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }

  getGradeReports(studentUID, studentSchooID) {

    let url = this.globals.url + "api_boletim.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)

  }

  getAllBills(studentUID, studentSchooID) {

    let url = this.globals.url + "api_boleto.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }

  getFinancialReports(studentUID, studentSchooID) {
    let url = this.globals.url + "api_demonstrativo.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }

  getFinancialCounts(studentUID, studentSchooID) {
    let url = this.globals.url + "api_contas.php";
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }

  getSchoolCalender(studentUID, studentSchooID) {
    let url = this.globals.url + 'api_agenda.php';
    let body = this.setBodyParams(studentUID, studentSchooID);
    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)

  }

  notificationReadUnread(lido, msgNumber, studentUID, studentSchooID) {
    let url = this.globals.url + 'api_ocorrencias_lido.php'
    // let body = new URLSearchParams();
    let body = this.setBodyParams(studentUID, studentSchooID);
    body.set('n', msgNumber)
    // body.set('l', lido)
    if (lido == '0') {
      body.set('l', '1')
    } else body.set('l', '0')

    let options = new RequestOptions({ headers: this.headers, search: body.toString() });
    console.log(body);
    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.catchError)
  }
  
  private setBodyParams(studentUID, studentSchooID) {

    let school_path = this.globals.school_path;
    let hash = this.globals.school_hash;
    let locatalTime = this.getLocaTime();
    let hashCode = md5(studentUID + studentSchooID + locatalTime.toString() + hash + school_path);

    let body = new URLSearchParams();
    body.set('c', school_path);
    body.set('m', studentSchooID);
    body.set('i', studentUID);
    body.set('d', locatalTime.toString());
    body.set('h', hashCode);
    body.set('r', this.globals.username)

    return body;
  }

  private getLocaTime() {
    let locatalTime = new Date().getTime() / 1000

    return parseInt(locatalTime.toString());
  }

  private logResponse(res: Response | any) {
    console.log(res.json());
  }

  private extractData(res: Response) {
    return res.json();
  }

  private catchError(error: Response | any) {
    return Observable.throw(error || "Server error.")
  }
}
