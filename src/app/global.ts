import { Injectable } from '@angular/core'

@Injectable()
export class GlobalVariable {
  public url: string = "http://www.kmescolar.net/kmescolar/api2/";
  public school_hash: string = "4fa09dd63fa11aad6b5d3fc60e813375bd1d3e52906799ec6a7e868f614d2e7dc302dcae6e72403340f34e0fc02c19277d92849407e20c0817ab428d78d405d6";
  public school_path: string = "teste";
  public loginData: any;
  public loginResponse: any;
  public student: any;
  public childPageData = {};
  public mainPageData: any;
  public username: string = '';

  public eraseGlobals() {
    this.loginData;
    this.loginResponse;
    this.student;
    this.childPageData = {};
    this.mainPageData;
    this.username = '';
  }
}