import { Component, ViewChild } from '@angular/core';
import { Platform, Events, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalVariable } from './global';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  parentName: any;
  parentPhoto: any;
  private data: any;

  constructor(platform: Platform,
    public events: Events,
    public menuCtrl: MenuController,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private nativeStorage: NativeStorage,
    private globals: GlobalVariable) {

    // if (globals.loginResponse) {

    //   let data = globals.loginData;
    //   this.parentName = data.NOME;
    //   this.parentPhoto = data.FOTO;
    //   console.log(globals.loginData);
    // }

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      nativeStorage.getItem("loginResponse").then((data) => {
        this.parentName = data.loginResponse.NOME;
        this.parentPhoto = data.loginResponse.FOTO;
        globals.loginResponse = data.loginResponse;
        globals.username = data.username;
        this.rootPage = "Child"
      }).catch(error => {
        console.log("user not already logged in");
        console.error(error);
        this.rootPage = "Login";
      })
      statusBar.styleDefault();
      splashScreen.hide();
      this.setupArticleMenuSubscribe();
    });
  }
  setupArticleMenuSubscribe() {
    console.log('setupArticleMenuSubscribe');
    // sub scribe to the populate event of the menu`
    this.events.subscribe('articleMenu:populate', data => {
      this.data = data;
      this.parentName = data.NOME;
      this.parentPhoto = data.FOTO;
    });
  }

  goToChildPage() {
    this.nav.setRoot("Child");
    this.menuCtrl.close();
  }
  changePassword() {
    this.nav.setRoot("ChangePassword");
    this.menuCtrl.close();
  }

  async logout() {
    await this.nativeStorage.clear()
    this.globals.eraseGlobals()
    await this.nav.setRoot("Login")
    this.menuCtrl.close()
  }
}

