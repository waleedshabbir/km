import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AuthService } from '../providers/auth-service';
import { DataService } from '../providers/data-service';
import { GlobalVariable } from './global';

import { MyApp } from './app.component';
import { ApiProvider } from '../providers/api';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    AuthService,
    DataService,
    GlobalVariable,
    PhotoViewer,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, ApiProvider
  ]
})
export class AppModule {}
